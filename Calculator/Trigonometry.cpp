#include "stdafx.h"
#include "Trigonometry.h"

struct MyStruct
{
	int a;
	int b;
};
Trigonometry::Trigonometry()
{
}
/**
* @brief This will calculate cos of given number
* @param a integer number for which cos need to calculate
*/
void Trigonometry::showCos(int a) {
	MyStruct* lpstMyStruct;
	lpstMyStruct->a = 1;
	cout << cos(a) << endl;;
}

/**
* @brief This will calculate Sin of given number
* @param a integer number for which Sin need to calculate
*/
void Trigonometry::showSin(int a) {
	cout << sin(a) << endl;;
}

/**
* @brief This will calculate tan of given number
* @param a integer number for which sin and cos need to calculate
*/
void Trigonometry::showAll(int a) {
	cout << "Sin of number" << endl; showSin(a);
	cout << "Cos of number" << endl; showCos(a);

}